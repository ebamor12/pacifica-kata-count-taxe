package services.impl;

import java.util.ArrayList;
import java.util.List;

import com.pacifica.calculetaxe.common.enumerations.ProductTypeEnum;
import com.pacifica.calculetaxe.models.*;

public class TestUtils {

    public static Invoice getInvoice() {
    	Invoice invoice = new Invoice();
        List<InvoiceDetail> invoiceDetails = new ArrayList<InvoiceDetail>();
        invoiceDetails.add(getChocolateOrderDetail());
        invoiceDetails.add(getBookOrderDetail());
        invoiceDetails.add(getOtherOrderDetail());
        invoice.setInvoiceDetails(invoiceDetails);
        invoice.setTaxeAmount(17.5);
        invoice.setTotalePriceAmount(118.5);
		return invoice;
      }

    public static InvoiceDetail getChocolateOrderDetail() {
    	InvoiceDetail invoiceDetail = new InvoiceDetail();
    	invoiceDetail.setPriceTTC(21);
    	invoiceDetail.setProduct(getChocolateProduct());
    	invoiceDetail.setQuantity(2);

		return invoiceDetail;
      }
    public static InvoiceDetail getBookOrderDetail() {
        InvoiceDetail invoiceDetail = new InvoiceDetail();
        invoiceDetail.setPriceTTC(27.5);
        invoiceDetail.setProduct(getChocolateProduct());
        invoiceDetail.setQuantity(2);

        return invoiceDetail;
    }

    public static InvoiceDetail getOtherOrderDetail() {
        InvoiceDetail invoiceDetail = new InvoiceDetail();
        invoiceDetail.setPriceTTC(70);
        invoiceDetail.setProduct(getOthersProduct());
        invoiceDetail.setQuantity(2);

        return invoiceDetail;
    }
    public static Order getOrder() {
        Order order = new Order();
        List<OrderDetail> orderDetail = new ArrayList<OrderDetail>();
        orderDetail.add(getFirstOrderLine());
        orderDetail.add(getSecondOrderLine());
        orderDetail.add(getThirdOrderLine());
        order.setOrderDetails(orderDetail);
   	return order;
      }

    public static OrderDetail getFirstOrderLine() {
        OrderDetail ligneCommandes = new OrderDetail();
        ligneCommandes.setProduct(getChocolateProduct());
        ligneCommandes.setQuantity(2);
        
		return ligneCommandes; 
      }
    public static OrderDetail getSecondOrderLine() {
        OrderDetail ligneCommandes = new OrderDetail();
        ligneCommandes.setProduct(getBookProduct());
        ligneCommandes.setQuantity(2);

        return ligneCommandes;
    }

    public static OrderDetail getThirdOrderLine() {
        OrderDetail ligneCommandes = new OrderDetail();
        ligneCommandes.setProduct(getOthersProduct());
        ligneCommandes.setQuantity(2);

        return ligneCommandes;
    }
    public static Product getChocolateProduct() {
    	
    	Product product = new Product();
    	product.setImported(true);
    	product.setLibelle("boîtes de chocolats");
    	product.setUnitPriceWithoutTaxe(10);
    	product.setProductType(ProductTypeEnum.FOOD);

    	return product;
    	
    }
    public static Product getBookProduct() {

        Product product = new Product();
        product.setImported(false);
        product.setLibelle("livres ");
        product.setUnitPriceWithoutTaxe(12.49);
       product.setProductType(ProductTypeEnum.BOOK);
        return product;

    }
    public static Product getOthersProduct() {

        Product product = new Product();
        product.setImported(true);
        product.setLibelle("2 flacons de parfum importé ");
        product.setUnitPriceWithoutTaxe(27.99);
        product.setProductType(ProductTypeEnum.OTHERS);
        return product;

    }

}
