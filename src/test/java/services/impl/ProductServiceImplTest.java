package services.impl;

import com.pacifica.calculetaxe.common.enumerations.ProductTypeEnum;
import com.pacifica.calculetaxe.models.Product;
import com.pacifica.calculetaxe.service.impl.ProductServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.pacifica.calculetaxe.common.constant.ProductNameConstant.*;

public class ProductServiceImplTest {
    ProductServiceImpl productServiceImpl;

    @Before
    public void setUp() {
        productServiceImpl = new ProductServiceImpl();
    }

    @Test
    public void should_create_prduct_from_orderline() {
        String orderLine="* 2 livres à 12.49€";
        int quantity =2;
        Product expectedProduct = TestUtils.getBookProduct();
        Product product =productServiceImpl.createProductFromOrderLine(orderLine, quantity);
        Assert.assertEquals(product.getProductType(),expectedProduct.getProductType());
        Assert.assertEquals(product.getUnitPriceWithoutTaxe(),expectedProduct.getUnitPriceWithoutTaxe(),0.0000d);

    }
    @Test
    public void should_return_book_as_type() {
        ProductTypeEnum productType =productServiceImpl.getTypeFromProductName(PRODUCT_NAME_BOOK);
        Assert.assertEquals( ProductTypeEnum.BOOK,productType);

    }
}
