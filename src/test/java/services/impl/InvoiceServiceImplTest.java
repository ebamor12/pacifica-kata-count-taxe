package services.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.pacifica.calculetaxe.models.Order;
import com.pacifica.calculetaxe.models.Invoice;
import com.pacifica.calculetaxe.service.impl.InvoiceServiceImpl;

public class InvoiceServiceImplTest {
	InvoiceServiceImpl invoiceServiceImpl;

	  @Before public void setUp() { 
		  invoiceServiceImpl = new InvoiceServiceImpl();
	  }
	
	@Test 
	public void should_create_invoice_from_order() {
	 Order order = TestUtils.getOrder();
	 Invoice expectedInvoice = TestUtils.getInvoice();
	 Invoice invoice = invoiceServiceImpl.createInvoiceFromOrder(order);

	 Assert.assertNotNull(invoice);
	 Assert.assertEquals(invoice.getTaxeAmount(), expectedInvoice.getTaxeAmount(), 0.00001d);
	 Assert.assertEquals(invoice.getTotalePriceAmount(), expectedInvoice.getTotalePriceAmount(), 0.00001d);
	}



}
