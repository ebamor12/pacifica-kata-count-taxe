package services.impl;

import com.pacifica.calculetaxe.models.Order;
import com.pacifica.calculetaxe.service.impl.OrderServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static com.pacifica.calculetaxe.common.constant.CommonConstant.FIRST_COMMAND_FILE_PATH;

public class OrderServiceImplTest {
    OrderServiceImpl orderServiceImpl;

    @Before
    public void setUp() {
        orderServiceImpl = new OrderServiceImpl();
    }

    @Test
    public void should_create_order_from_file() throws IOException {
        Order order = orderServiceImpl.createOrderFromFile(FIRST_COMMAND_FILE_PATH);
        Order expectedOrder = TestUtils.getOrder();
        Assert.assertEquals(order.getOrderDetails().size(), expectedOrder.getOrderDetails().size());
    }
}
