package services.impl;

import com.pacifica.calculetaxe.models.*;
import com.pacifica.calculetaxe.service.impl.TaxeServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TaxeServiceImplTest {

    TaxeServiceImpl taxeServiceImpl;

    @Before
    public void setUp() {
        taxeServiceImpl = new TaxeServiceImpl();
    }

    @Test
    public void should_return_taxe_for_imported_food() {
        Product product = TestUtils.getChocolateProduct();
        double totalProductPriceWithoutTaxe = 20;
        double taxe = taxeServiceImpl.countTaxeOfProduct(product,totalProductPriceWithoutTaxe);
        double expectedTaxeOfOneProduct = 1;
        Assert.assertEquals(taxe, expectedTaxeOfOneProduct, 0.00001d);

    }
    @Test
    public void should_return_taxe_of_book_product() {
        Product product = TestUtils.getBookProduct();
        double totalProductPriceWithoutTaxe = 24.98;
        double taxe = taxeServiceImpl.countTaxeOfProduct(product,totalProductPriceWithoutTaxe);
        double expectedTaxeOfOneProduct = 2.5;
        Assert.assertEquals(taxe, expectedTaxeOfOneProduct, 0.00001d);

    }

    @Test
    public void should_return_taxe_of_other_product() {
        Product product = TestUtils.getOthersProduct();
        double totalProductPriceWithoutTaxe = 55.98;
        double taxe = taxeServiceImpl.countTaxeOfProduct(product,totalProductPriceWithoutTaxe);
        double expectedTaxeOfOneProduct = 14;
        Assert.assertEquals(taxe, expectedTaxeOfOneProduct, 0.00001d);

    }

    @Test
    public void should_return_price_TTC_of_other_product(){
        OrderDetail orderDetail = TestUtils.getOtherOrderDetail();
        double priceTTC = taxeServiceImpl.countProductTotalPriceTTC(orderDetail);
        double expectedPriceTTC = 70;
        Assert.assertEquals(priceTTC, expectedPriceTTC, 0.00001d);
    }
}
