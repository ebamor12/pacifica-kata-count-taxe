import com.pacifica.calculetaxe.models.Invoice;
import com.pacifica.calculetaxe.service.impl.InvoiceServiceImpl;
import com.pacifica.calculetaxe.service.impl.OrderServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static com.pacifica.calculetaxe.common.constant.CommonConstant.FIRST_COMMAND_FILE_PATH;
import static com.pacifica.calculetaxe.common.constant.CommonConstant.FIRST_EXPECTED_OUTPUT_FILE_PATH;

public class AcceptanceTestOrder {
    InvoiceServiceImpl invoiceServiceImpl;
    OrderServiceImpl orderServiceImpl;

    @Before
    public void setUp() {
        invoiceServiceImpl = new InvoiceServiceImpl();
        orderServiceImpl = new OrderServiceImpl();
    }

    @Test
    public void should_create_invoice_file() throws IOException {
        Invoice invoice1 = invoiceServiceImpl
                .createInvoiceFromOrder(orderServiceImpl.createOrderFromFile(FIRST_COMMAND_FILE_PATH));
        List<String> expectedLines = Files.lines(Paths.get(FIRST_EXPECTED_OUTPUT_FILE_PATH)).collect(Collectors.toList());
        List<String> lines = Files.lines(Paths.get(invoiceServiceImpl.createInvoiceFile(invoice1))).collect(Collectors.toList());
        Assert.assertEquals(expectedLines,lines);
    }
}
