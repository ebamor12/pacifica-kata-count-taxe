package com.pacifica.calculetaxe.common.constant;

public class ProductNameConstant {
    public static final String PRODUCT_NAME_BOOK                                ="livre";
    public static final String PRODUCT_NAME_MANY_BOOK                           ="livres";
    public static final String PRODUCT_NAME_CHOCOLAT                            ="chocolat";
    public static final String PRODUCT_NAME_MANY_CHOCOLAT                       ="chocolats";
    public static final String PRODUCT_NAME_CHOCOLATE_BARS                      ="barres de chocolat";
    public static final String PRODUCT_NAME_CHOCOLATE_BOXES                     ="boîtes de chocolats";
    public static final String PRODUCT_NAME_PILL                                ="pilule";
    public static final String PRODUCT_NAME_MANY_PILL                           ="pilules";
    public static final String PRODUCT_NAME_PILL_BOXES                          ="boîtes de pilules contre la migraine";
    public static final String PRODUCT_NAME_PERFUME                             ="parfum";
    public static final String PRODUCT_NAME_CD                                  ="CD musical";
    public static final String PRODUCT_NAME_PERFUME_BOTTLES                     ="flacons de parfum";
    public static final String PRODUCT_NAME_PERFUME_BOTTLE                      ="flacon de parfum";

    private ProductNameConstant() {
    }
}
