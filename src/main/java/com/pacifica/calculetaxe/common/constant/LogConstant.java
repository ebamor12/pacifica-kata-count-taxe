package com.pacifica.calculetaxe.common.constant;

public class LogConstant {

    public static final String CREATE_ORDER_LOG                                ="Create order from file: ";
    public static final String INEXIST_FILE_LOG                                =" does not exist";
    public static final String COUNT_TAXE_LOG                                  ="count the taxe of each type";
    public static final String COUNT_PRICE_TTC_LOG                             ="count the price TTC of the product";
    public static final String CREATE_INVOICE_LOG                              ="Create invoice from created order";
    public static final String ORDER_NULL_LOG                                  ="invoice is not created because order is null";
    public static final String CREATE_INVOICE_DETAIL_LOG                       ="invoice detail is created";
    public static final String CREATE_INVOICE_FILE_LOG                         ="Create invoice file with totale TTC: ";
    public static final String FILE_EXIST_LOG                                  ="File already exist at location: " ;
    public static final String INVOICE_NULL_LOG                                ="file can not be created because invoice is null ";

    private LogConstant() {
    }
}
