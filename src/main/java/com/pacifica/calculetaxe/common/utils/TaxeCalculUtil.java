package com.pacifica.calculetaxe.common.utils;

import static com.pacifica.calculetaxe.common.constant.CountConstant.*;
public class TaxeCalculUtil {
	
	public static double roundTaxe(double value) {

		double roundValue;
		double roundToTwoDecimal = Math.floor(value * HUNDRED) / HUNDRED ;
		String roundToTwoDecimalStr = String.valueOf(roundToTwoDecimal);
		String lastNumberStr = roundToTwoDecimalStr.substring(roundToTwoDecimalStr.indexOf(".") + 2);

		Integer lastNumber = (lastNumberStr.isBlank() ? ZERO_PERCENT : Integer.valueOf(lastNumberStr));

		if (lastNumber == ZERO_PERCENT || lastNumber == FIVE) {
			roundValue = roundToTwoDecimal;
		}

		else if (lastNumber < FIVE && lastNumber > ZERO_PERCENT) {
			String strWithoutLastNumber = roundToTwoDecimalStr.substring(ZERO_PERCENT, roundToTwoDecimalStr.indexOf(".") + TWO);
			String finalString = strWithoutLastNumber.concat("5");
			roundValue = Double.valueOf(finalString);
		}

		else {
			roundValue = Math.round(roundToTwoDecimal * TEN_DOUBLE) / TEN_DOUBLE;
		}
		
		return roundValue;
		
	}

	public static double roundPriceTTC(double value) {

		double roundValue ;
		String roundToTwoDecimalStr = String.valueOf(value);
		String lastNumberStr = roundToTwoDecimalStr.substring(roundToTwoDecimalStr.indexOf(".") + TWO);

		Integer lastNumber = (lastNumberStr.isBlank() ? ZERO_PERCENT : Integer.valueOf(lastNumberStr));
		if (lastNumber == FIVE) {
			roundValue = value;
		}
		else  {
			roundValue = Math.round(value * TEN_DOUBLE) / TEN_DOUBLE;
		}
		return roundValue;
		
	}
	
	public static double formatDoubleToTwoDecimalPlaces(double value){
		String number = String.valueOf(value);
		return number.contains(".") && number.length() > THREE+number.indexOf(".") ?
				Double.parseDouble(number.substring(ZERO_PERCENT, number.indexOf(".")+THREE)) : value;
	}

	private TaxeCalculUtil() {
	}
}
