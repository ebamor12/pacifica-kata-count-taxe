package com.pacifica.calculetaxe.common.utils;

import com.pacifica.calculetaxe.models.Product;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.pacifica.calculetaxe.common.constant.CommonConstant.A_IN_LINE_COMMAND;
import static com.pacifica.calculetaxe.common.constant.CommonConstant.IMPORT_IN_LINE_COMMAND;

public class ProductUtil {

    public static int getQuantityOfProduct(String lineCommand) {
        Matcher matcher = Pattern.compile("\\d+").matcher(lineCommand);
        matcher.find();
        return Integer.valueOf(matcher.group());
    }

    public static String substringLine(String lineCommand, String startSubstring, String endSubstring) {
        int indexofStartSubstring = lineCommand.indexOf(startSubstring) + 2;
        int indexofEndSubstring = lineCommand.indexOf(endSubstring);
        return lineCommand.substring(indexofStartSubstring, indexofEndSubstring);
    }

    public static String getProductLibelle(Product product, String orderLine){
       if(product.isImported()) {
            return (ProductUtil.substringLine(orderLine, String.valueOf(getQuantityOfProduct(orderLine)), orderLine.substring(orderLine.indexOf(IMPORT_IN_LINE_COMMAND),orderLine.indexOf(A_IN_LINE_COMMAND)))).trim();
        }
        else{
            return (ProductUtil.substringLine(orderLine,  String.valueOf(getQuantityOfProduct(orderLine)), A_IN_LINE_COMMAND)).trim();
        }
    }

    private ProductUtil() {
    }
}
