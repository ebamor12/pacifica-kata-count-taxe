package com.pacifica.calculetaxe.common.utils;

import com.pacifica.calculetaxe.common.constant.CommonConstant;
import com.pacifica.calculetaxe.models.Invoice;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileUtil {
    public static File createNewFile(Invoice invoice){
        Date date = new Date() ;
        SimpleDateFormat dateFormat = new SimpleDateFormat(CommonConstant.DATE_PATTERN) ;
        return new File(CommonConstant.TEST_PATH+ CommonConstant.FILE_NAME+ invoice.getTotalePriceAmount()+"_"+dateFormat.format(date)+ CommonConstant.TXT_EXTENSION) ;
    }

    public static void createTextInFile(File file, String text){
        try (FileWriter fw = new FileWriter(file, true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {
             out.println(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private FileUtil() {
    }
}
