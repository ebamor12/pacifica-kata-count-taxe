package com.pacifica.calculetaxe;

import com.pacifica.calculetaxe.common.constant.CommonConstant;
import com.pacifica.calculetaxe.models.Invoice;
import com.pacifica.calculetaxe.service.InvoiceService;
import com.pacifica.calculetaxe.service.OrderService;
import com.pacifica.calculetaxe.service.impl.InvoiceServiceImpl;
import com.pacifica.calculetaxe.service.impl.OrderServiceImpl;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException {
        OrderService orderServiceImpl = new OrderServiceImpl();
        InvoiceService invoiceServiceImpl = new InvoiceServiceImpl();

        Invoice invoice1 = invoiceServiceImpl
                .createInvoiceFromOrder(orderServiceImpl.createOrderFromFile(CommonConstant.FIRST_COMMAND_FILE_PATH));
        Invoice invoice2 = invoiceServiceImpl
                .createInvoiceFromOrder(orderServiceImpl.createOrderFromFile(CommonConstant.SECOND_COMMAND_FILE_PATH));
        Invoice invoice3 = invoiceServiceImpl
                .createInvoiceFromOrder(orderServiceImpl.createOrderFromFile(CommonConstant.THIRD_COMMAND_FILE_PATH));

        invoiceServiceImpl.createInvoiceFile(invoice1);
        invoiceServiceImpl.createInvoiceFile(invoice2);
        invoiceServiceImpl.createInvoiceFile(invoice3);
    }
}
