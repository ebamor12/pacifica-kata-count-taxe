package com.pacifica.calculetaxe.models;

public class OrderDetail {
	
	private Integer quantity;
	private Product product;
	private String importLibelle;

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}

	public String getImportLibelle() {
		return importLibelle;
	}

	public void setImportLibelle(String importLibelle) {
		this.importLibelle = importLibelle;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
}
