package com.pacifica.calculetaxe.models;

public class InvoiceDetail extends OrderDetail {
	

	private double priceTTC;


	public double getPriceTTC() {
		return priceTTC;
	}

	public void setPriceTTC(double priceTTC) {
		this.priceTTC = priceTTC;
	}


}
