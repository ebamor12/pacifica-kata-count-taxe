package com.pacifica.calculetaxe.service;

import com.pacifica.calculetaxe.models.Order;

public interface OrderService {

    Order createOrderFromFile(String fileConfigPath);

}
