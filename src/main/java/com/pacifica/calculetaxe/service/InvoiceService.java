package com.pacifica.calculetaxe.service;

import com.pacifica.calculetaxe.models.Order;
import com.pacifica.calculetaxe.models.Invoice;

import java.io.IOException;

public interface InvoiceService {

	Invoice createInvoiceFromOrder(Order order);
	String createInvoiceFile(Invoice invoice) throws IOException;
}
