package com.pacifica.calculetaxe.service.impl;

import com.pacifica.calculetaxe.common.utils.TaxeCalculUtil;
import com.pacifica.calculetaxe.models.OrderDetail;
import com.pacifica.calculetaxe.models.Product;
import com.pacifica.calculetaxe.service.TaxeService;

import java.util.logging.Logger;

import static com.pacifica.calculetaxe.common.constant.CountConstant.*;
import static com.pacifica.calculetaxe.common.constant.LogConstant.*;

public class TaxeServiceImpl implements TaxeService {

    private static final Logger LOGGER = Logger.getLogger(TaxeServiceImpl.class.getName());

    @Override
    public double countProductTotalPriceTTC(OrderDetail orderDetail) {
        LOGGER.info(COUNT_PRICE_TTC_LOG );
        double totalePriceTTC =countTotalProductPriceWithoutTaxe(orderDetail) +
                countTaxeOfProduct(orderDetail.getProduct(), countTotalProductPriceWithoutTaxe(orderDetail));
        return TaxeCalculUtil.roundPriceTTC(
                TaxeCalculUtil.formatDoubleToTwoDecimalPlaces(totalePriceTTC));
    }

    @Override
    public double countTaxeOfProduct(Product product, double totalPriceWithouTaxe) {
        LOGGER.info(COUNT_TAXE_LOG );
        double importedProductTaxe = (product.isImported()
                ? this.countTaxeAmount(totalPriceWithouTaxe, FIVE)
                : ZERO_PERCENT);

        return this.countTaxeAmount(totalPriceWithouTaxe, product.getProductType().getTax())+importedProductTaxe;
    }

    @Override
    public double countTotalProductPriceWithoutTaxe(OrderDetail orderDetail) {
        return orderDetail.getQuantity() * orderDetail.getProduct().getUnitPriceWithoutTaxe();
    }

   private double countTaxeAmount(double prixTotaleHorsTaxe, int taxePercent) {
        return TaxeCalculUtil.roundTaxe((prixTotaleHorsTaxe * taxePercent)/HUNDRED);
    }


}
