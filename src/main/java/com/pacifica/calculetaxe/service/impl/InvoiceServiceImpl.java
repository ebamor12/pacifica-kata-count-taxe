package com.pacifica.calculetaxe.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.pacifica.calculetaxe.common.constant.CommonConstant;
import com.pacifica.calculetaxe.common.utils.FileUtil;
import com.pacifica.calculetaxe.models.*;
import com.pacifica.calculetaxe.service.TaxeService;
import com.pacifica.calculetaxe.service.InvoiceService;

import static com.pacifica.calculetaxe.common.constant.LogConstant.*;
import static com.pacifica.calculetaxe.common.constant.CommonConstant.*;

public class InvoiceServiceImpl implements InvoiceService {

    private double montantTaxeTotale;
    private double prixTotaleTTC;
    private static final Logger LOGGER = Logger.getLogger(InvoiceServiceImpl.class.getName());

    TaxeService taxeServiceImpl = new TaxeServiceImpl();

    public Invoice createInvoiceFromOrder(Order order) {

        montantTaxeTotale = 0.0d;
        prixTotaleTTC = 0.0d;
        if(order!=null) {
            LOGGER.info(CREATE_INVOICE_LOG );
            List<InvoiceDetail> invoiceDetailList = order.getOrderDetails().stream()
                    .map(line -> {
                        this.countTotalPriceTTC(line);
                        this.countTotalTaxe(line);
                        return createInvoiceDetail(line);})
                    .collect(Collectors.toList());

            return this.createInvoice(invoiceDetailList, montantTaxeTotale, prixTotaleTTC);
        }
        else{
            LOGGER.info(ORDER_NULL_LOG );
            return null;
        }

    }

    private InvoiceDetail createInvoiceDetail(OrderDetail orderDetail){
        LOGGER.info(CREATE_INVOICE_DETAIL_LOG );
        return this.createInvoiceLine(taxeServiceImpl.countProductTotalPriceTTC(orderDetail), orderDetail);
    }

    private double countTotalPriceTTC(OrderDetail orderDetail){
        prixTotaleTTC += taxeServiceImpl.countProductTotalPriceTTC(orderDetail);
        return prixTotaleTTC;
    }

    private double countTotalTaxe(OrderDetail orderDetail){
        montantTaxeTotale += taxeServiceImpl.countTaxeOfProduct(
                orderDetail.getProduct(), taxeServiceImpl.countTotalProductPriceWithoutTaxe(orderDetail));
        return montantTaxeTotale;
    }

    private InvoiceDetail createInvoiceLine(double prixProductTTCRound, OrderDetail orderDetail) {
        InvoiceDetail invoiceDetail = new InvoiceDetail();
        invoiceDetail.setPriceTTC(prixProductTTCRound);
        invoiceDetail.setProduct(orderDetail.getProduct());
        invoiceDetail.setQuantity(orderDetail.getQuantity());
        invoiceDetail.setImportLibelle(orderDetail.getImportLibelle());
        return invoiceDetail;
    }

    private Invoice createInvoice(List<InvoiceDetail> invoiceDetail, double montantTaxeTotale, double prixTotaleTTC) {
        Invoice invoice = new Invoice();
        invoice.setInvoiceDetails(invoiceDetail);
        invoice.setTaxeAmount(montantTaxeTotale);
        invoice.setTotalePriceAmount(prixTotaleTTC);
        return invoice;
    }

    public String createInvoiceFile(Invoice invoice) throws IOException {
        if (invoice != null) {
            LOGGER.info(CREATE_INVOICE_FILE_LOG + invoice.getTotalePriceAmount());
            File file = FileUtil.createNewFile(invoice);
            if (file.createNewFile()) {
                FileUtil.createTextInFile(file, this.createInvoiceText(invoice));
            } else {
                LOGGER.info(FILE_EXIST_LOG+ file.getCanonicalPath());
            }
            return file.getCanonicalPath();
        } else {
            LOGGER.info(INVOICE_NULL_LOG);
            return null;
        }
    }

    private String createInvoiceText(Invoice invoice) {

        List<String> invoiceProductLine = createInvoiceLinesText(invoice);
        String globalTaxe = TAXE_TOTAL + invoice.getTaxeAmount() + EURO_IN_LINE_COMMAND;
        String globalPrice = TOTAL_PRIX + invoice.getTotalePriceAmount() + EURO_IN_LINE_COMMAND;

        return String.join("\n", invoiceProductLine) + "\n\n" + globalTaxe + "\n" + globalPrice;
    }

    private List<String> createInvoiceLinesText(Invoice invoice) {
        List<String> invoiceProductLine = new ArrayList<>();
        if (invoice != null) {
            invoiceProductLine = invoice.getInvoiceDetails().stream()
                    .map(line -> createInvoiceLineText(line))
                    .collect(Collectors.toList());
        }
        return invoiceProductLine;
    }

    private  String createInvoiceLineText(InvoiceDetail invoiceDetail) {

        return START_INVOICE_STRING + invoiceDetail.getQuantity() + SPACE_STRING + invoiceDetail.getProduct().getLibelle() +invoiceDetail.getImportLibelle()+ A_ORDER
                + invoiceDetail.getProduct().getUnitPriceWithoutTaxe() + EURO_IN_INVOICE
                + invoiceDetail.getPriceTTC() + CommonConstant.EURO_IN_LINE_COMMAND;
    }


}
