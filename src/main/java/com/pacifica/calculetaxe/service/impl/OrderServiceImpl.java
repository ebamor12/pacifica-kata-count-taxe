package com.pacifica.calculetaxe.service.impl;

import com.pacifica.calculetaxe.common.utils.ProductUtil;
import com.pacifica.calculetaxe.models.Order;
import com.pacifica.calculetaxe.models.OrderDetail;
import com.pacifica.calculetaxe.service.OrderService;
import com.pacifica.calculetaxe.service.ProductService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.pacifica.calculetaxe.common.constant.CommonConstant.*;
import static com.pacifica.calculetaxe.common.constant.LogConstant.*;

public class OrderServiceImpl implements OrderService {

    ProductService productServiceImpl = new ProductServiceImpl();

    private static final Logger LOGGER = Logger.getLogger(OrderServiceImpl.class.getName());

    public Order createOrderFromFile(String fileConfigPath) {
        LOGGER.info(CREATE_ORDER_LOG + fileConfigPath);

        Order order = new Order();
        if (Files.exists(Paths.get(fileConfigPath))) {
            try (Stream<String> fileLines = Files.lines(Paths.get(fileConfigPath))) {
                List<OrderDetail> orderDetails = fileLines
                        .filter(Objects::nonNull)
                        .map(line -> createOrderDetailFromOrderLine(line))
                        .collect(Collectors.toList());
                order.setOrderDetails(orderDetails);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return order;
        }
        else {
            LOGGER.info(fileConfigPath + INEXIST_FILE_LOG);
            order = null;
        }
        return order;
    }

    private OrderDetail createOrderDetailFromOrderLine(String orderLine) {

        OrderDetail orderDetail = new OrderDetail();
        int productQuantity = ProductUtil.getQuantityOfProduct(orderLine);
        orderDetail.setProduct(productServiceImpl.createProductFromOrderLine(orderLine, productQuantity));
        orderDetail.setQuantity(productQuantity);
        orderDetail.setImportLibelle(this.getImportLibelle(orderLine, orderDetail));

        return orderDetail;
    }

   private String getImportLibelle(String orderLine, OrderDetail orderDetail){
        return orderDetail.getProduct().isImported() ?
                orderLine.substring(orderLine.indexOf(IMPORT_IN_LINE_COMMAND),(orderLine.indexOf(A_ORDER))) : EMPTY_STRING;
    }


}
